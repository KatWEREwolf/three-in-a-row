import os

FILLCOUNT = 0
VERSIA = -1
class Turn:

    turn_dic = {"O": "red", "X":"blue"}

    def __init__(self, turn = "O"):
        self.__state = self.turn_dic[turn]

    def next_turn(self):
        if self.__state == self.turn_dic["O"]:
            print("Next turn is: blue")
            self.__state = self.turn_dic["X"]
        else:
            print("Next turn is: red")
            self.__state = self.turn_dic["O"]

    def cur_turn(self):
        return self.__state

class Cell:

    __empty_cell = "_ | "

    def __init__(self, state = __empty_cell):
        self.__state = state

    def is_empty(self):
        return self.__state == self.__empty_cell

    def set(self, state):
        if self.is_empty():
            self.__state = state
            return True
        else:
            return False

    def state(self):
        return self.__state

class Field:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        self.__cells = []
        for i in range(0, x):
            arr = []
            for j in range(0, y):
                cell = Cell()
                arr.append(cell)
            self.__cells.append(arr)

    def show(self):
        os.system('cls')
        for x in range(0, self.__x):
            row = ""
            for y in range(0, self.__y):
                row += self.__cells[x][y].state()
            print(row)

    def get_cell(self, x, y):
        return self.__cells[x, y]

    def set_cell(self, x, y, turn):
        global FILLCOUNT
        FILLCOUNT+=1
        return self.__cells[x][y].set(turn)

    def get_arr(self):
        os.system('cls')
        arr = []
        for x in range(0, self.__x):
            row = []
            for y in range(0, self.__y):
                row.append(self.__cells[x][y].state())
            arr.append(row)
        return arr


class Game:

    def __init__(self, first_turn):
        width = False
        height = False
        self.__t = Turn(first_turn)
        self.__width = width
        self.__height = height
        while not self.__width:
            x = self.input_data("Type field width(from 3 to 100): ")
            if x > 2 and x < 101:
                self.__width = x
            else:
                print("incorrect input")
        if VERSIA == 1:
            while not self.__height:
                y = self.input_data("Type field height(from 3 to 100): ")
                if y > 2 and y < 101:
                    self.__height = y
                else:
                    print("incorrect input")
        else:
            y = x
            self.__height = x
        success = False
        while not success:
            self.__l = self.input_data("Type length of sequence to win (l <= min(x, y):")
            success = self.__l <= min(x, y)
        self.__f = Field(x, y)
        self.run()

    def input_data(self, invitation):
        while True:
            try:
                print(invitation)
                return int(input())
            except (ValueError, EOFError):
                print("incorrect input")
                
    def isInField(self, x, y):
        if self.__width > x and self.__height > y and -(self.__width) - 1 < x and -(self.__height) - 1 < y:
            return True

        else:
            return False
        
    def someone_won(self,turnColor,lenghtToWin,Field):
        
        
        for i in range(self.__width):
            count = 0
            for j in range(self.__height):
                if Field[i][j] == turnColor:
                    count+=1
                else:
                    if j < lenghtToWin - 1:
                        continue
                    else:
                        count = 0
                if count >= lenghtToWin:
                    return True
                    break
    
        for i in range(self.__height):
            count=0
            for j in range(self.__width):
                if Field[j][i] == turnColor:
                    count+=1
                else:
                    if j < lenghtToWin - 1:
                        continue
                    else:
                        count = 0
                if count >= lenghtToWin:
                    return True
                    break
                
        #диагональ 
        if VERSIA == 2:       
            for j in range(self.__width):
                count = 0
                i = 0
                while (self.isInField(i,i+j)):
                    if(Field[i+j][i]) != turnColor:
                        count = 0
                    else:
                        count+=1
                        if count >= lenghtToWin:
                            return True
                    i+=1

            for j in range(1,self.__height):
                count = 0
                i = 0
                while (self.isInField(i+j,i)):
                    if(Field[i][i+j]) != turnColor:
                        count = 0
                    else:
                        count+=1
                        if count >= lenghtToWin:
                            return True
                    i+=1

            for j in range(self.__width):
                count = 0
                i = 0
                while (self.isInField(-i-1,i+j)):
                    if Field[i+j][-i-1] != turnColor:
                        count = 0
                    else:
                        count+=1
                        if count >= lenghtToWin:
                            return True
                    i+=1

            for j in range(0, self.__height-1):
                count = 0
                i = 0
                while (self.isInField(j-i,i)):
                    if Field[i][j-i] != turnColor:
                        count = 0
                    else:
                        count+=1
                        if count >= lenghtToWin:
                            print("Но это неточно")
                            return True
                    i+=1

        if FILLCOUNT == self.__height*self.__width:
            print(" Ничья ")
            return True

        return False
                
    def run(self):
        while not self.someone_won(self.__t.cur_turn(),self.__l,self.__f.get_arr()):
            self.__f.show()
            self.turn()
        print("The winner is ", self.__t.cur_turn())
        
    def turn(self):
        success = False
        while not success:
            check_row = False
            check_column = False
            while not check_row:
                x = self.input_data("Type cell row:") - 1
                if x > -1 and x < self.__width:
                    check_row = True
                else:
                    print("incorrect input")
            while not check_column:
                y = self.input_data("Type cell column:") - 1
                if y > -1 and y < self.__height:
                    check_column = True
                else:
                    print("incorrect input")
            success = self.__f.set_cell(x, y, self.__t.cur_turn())
            if not success:
                print("Cell is not empty.")
        self.__t.next_turn()

print("WELCOME")
versia = False
while not versia:
    print("Choose type of game: 1 - any fild, you cannot win using diagonal, 2- squere fild, you can win using diagonal")
    choose_type = int(input())
    if choose_type == 1:
        VERSIA = 1
        versia = True
    elif choose_type == 2:
        VERSIA = 2
        versia = True
    else:
        print("incorrect input")
check_turn_input = False
while not check_turn_input:
    print("Who start the game (type O or X):")
    first_turn = input()
    if first_turn == "O" or first_turn == "X":
        check_turn_input = True
    else:
        print("incorrect input")
    
g = Game(first_turn)
